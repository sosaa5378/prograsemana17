from django.contrib import admin
from django.urls import path
from universidad.views import univer

urlpatterns = [
    path('admin/', admin.site.urls),
    path('univer/', univer),
]
