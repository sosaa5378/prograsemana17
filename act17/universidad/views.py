from django.shortcuts import render
from .models import Carrera, Estudiante, Profesor, Asignatura

def univer(request):
    carreras = Carrera.objects.all()
    estudiantes = Estudiante.objects.all()
    profesores = Profesor.objects.all()
    asignaturas = Asignatura.objects.all()

    return render(request, 'heh.html', {
        'carreras': carreras,
        'estudiantes': estudiantes,
        'profesores': profesores,
        'asignaturas': asignaturas,
    })
