from django.db import models

class Carrera(models.Model):
    ID_Carrera = models.IntegerField(primary_key=True)
    Nombre_Carrera = models.CharField(max_length=255)

class Estudiante(models.Model):
    ID_Estudiante = models.IntegerField(primary_key=True)
    Nombre = models.CharField(max_length=255)
    Apellido = models.CharField(max_length=255)
    Edad = models.IntegerField()
    Carrera_ID = models.ForeignKey(Carrera, on_delete=models.CASCADE)

class Profesor(models.Model):
    ID_Profesor = models.IntegerField(primary_key=True)
    Nombre = models.CharField(max_length=255)
    Apellido = models.CharField(max_length=255)
    Correo = models.CharField(max_length=255)

class Asignatura(models.Model):
    ID_Asignatura = models.IntegerField(primary_key=True)
    Nombre_Asignatura = models.CharField(max_length=255)
    ID_Profesor = models.ForeignKey(Profesor, on_delete=models.CASCADE)
    Carrera_ID = models.ForeignKey(Carrera, on_delete=models.CASCADE)